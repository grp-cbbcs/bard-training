# Local Installation of the BARD desktop

These instructions helps you set up a local version of the Bioimage Analysis Research Desktop(BARD) on top of Kubernetes.

## Requirements
1. Ubuntu 22
2. Minimus 20G disk space
3. x86-64 architecture


## 1. [Install Kubernetes on Local computer](https://git.embl.de/grp-cbbcs/bard-training/-/blob/main/01_Kubernetes_Instalation.md?ref_type=heads)
## 2. [Set up BARD for Kubernetes](https://git.embl.de/grp-cbbcs/bard-training/-/blob/main/02_BARD_Installation.md)